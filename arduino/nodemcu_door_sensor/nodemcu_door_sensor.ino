//#define _DEBUG_
#define _DISABLE_TLS_

#include <SPI.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>

#define WIFI_SSID "__ssid__"
#define WIFI_PASS "__pass__"
#define API_PATH "http://192.168.xxx.xxx/sensor.php?evt=dooropen"

WiFiClient wifiClient;
HTTPClient http;

int wifiEnabled = 0;
int notifyctr = 0;
int state; // 0 = off (open circuit); 1 = on (closed circuit)

void setup() {
  Serial.begin(115200);
  pinMode(D5, INPUT_PULLUP);

  connectWifi();
}


void loop() {

  delay(500);

  state = digitalRead(D5);
  
  if (state == HIGH && wifiEnabled == 1)
  {
    notifyctr += 1;
    
    if(notifyctr == 1)
    {
      http.begin(wifiClient, API_PATH);
      http.GET();
      http.end();
    }
  }
  else {
    notifyctr = 0;
  }
  
}


void connectWifi()
{
    WiFi.mode(WIFI_STA);
    WiFi.begin(WIFI_SSID, WIFI_PASS);

    while(WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
    }

    wifiEnabled = 1;
}