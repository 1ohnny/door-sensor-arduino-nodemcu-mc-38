# Door sensor on Arduino NodeMcu with MC-38 and send notification via WiFi to HTTP server

Arduino project for detection of door open with MC-38 wired magnetic sensor on NodeMcu with ESP8266Wifi and ESP8266HTTPClient libraries.
It sends notification via WiFi and HTTP GET request to remote HTTP server. Notification is sent instantly and only once after every door open.

## Required hardware:

1. NodeMcu unit
2. MC-38 magnetic sensor
3. 2x pins (without wires)

## Connection

1. Get your pins and connect them to wires of MC-38 (or you can directly solder it to pins of your NodeMcu unit, but be carefull)
2. Connect one pin to GND
3. Connect second pin to one of digital pins (in this project it is defined to **D5**, but you can change it in arduino project file by replacing in setup() function)

## Configure and upload

1. Open arduino project file in your Arduino studio.
2. Don't forget to set correct Board in your Tools menu: **NodeMcu 1.0**. 
3. Write your WiFi credentials before upload into constants **WIFI_SSID** and **WIFI_PASS**.
4. Define absolute path to script on your local/remote web server into constant **API_PATH**. You can also use local network IP address of another device, but it should have assigned static IP address in your network.
5. Verify & upload project file into your NodeMcu unit with Arduino studio.
